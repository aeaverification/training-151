 This 5% Public Use Microdata Sample Equivalency (PUMEQ5) file is intended to help users understand the relationships        
 between standard Census 2000 geographic concepts (counties, county subdivisions, places, and census tracts) and the         
 SuperPUMA and PUMA geographic units of the 5% PUMS data files.                                                              
                                                                                                                             
 Each PUMEQ5 file delineates the summary level codes specific to the PUMS geography, and has its own set of summary          
 level code and hierarchical sequencing, as described below:                                                                 
                                                                                                                             
 780 State-SuperPUMA-PUMA                                                                                                    
     781 State-SuperPUMA-PUMA-County (or part)                                                                               
         782 State-SuperPUMA-PUMA-County-County Subdivision (or part) (20 states with functioning county subdivisions)       
             783 State-SuperPUMA-PUMA-County-County Subdivision-Place/Remainder (or part)                                    
                 784 State-SuperPUMA-PUMA-County-County Subdivision-Place/Remainder-Census Tract                             
                                                                                                                             
 Please note that in the New England states, there may be more than one summary level 781 record within a single             
 PUMA, since these areas may exist in more than one metropolitan area/nonmetropolitan area.  Please also                     
 note that county subdivision codes are not used in the 30 states, the District of Columbia, and Puerto Rico,                
 that have no functioning county subdivisions. Census tracts are shown only when a place has parts in more than              
 one PUMA.                                                                                                                   
                                                                                                                             
 The fields and content of the PUMEQ5 file is described below:                                                               
                                                                                                                             
   FIELD     DESCRIPTION                                                                                                     
                                                                                                                             
     A       Summary Level Code (described above)                                                                            
     B       FIPS State Code                                                                                                 
     C       SuperPUMA Code                                                                                                  
     D       PUMA Code                                                                                                       
     E       FIPS County Code                                                                                                
     F       FIPS County Subdivision Code                                                                                    
     G       FIPS Place Code                                                                                                 
     H       Central City Indicator:                                                                                         
               0 = Not in central city                                                                                       
               1 = In central city                                                                                           
     I       Census Tract Code                                                                                               
     J       Metropolitan Statistical Area/Consolidated Statistical Area Code                                                
     K       Primary Metropolitan Statistical Area Code                                                                      
     L       Census 2000 100% Population Count                                                                               
     M       Area Name                                                                                                       
                                                                                                                             
                                                                                                                             
   A  B     C     D   E     F     G H      I    J    K        L M                                                            
                                                                                                                             
 780 56 56100 00100                                      121885 PUMA 00100                                                   
 781 56 56100 00100 023                      9999         14573 Lincoln County                                               
 783 56 56100 00100 023       00245 0        9999          1818 Afton town                                                   
 783 56 56100 00100 023       01695 0        9999           550 Alpine town                                                  
 783 56 56100 00100 023       01888 0        9999            82 Alpine Northeast CDP                                         
 783 56 56100 00100 023       01936 0        9999           152 Alpine Northwest CDP                                         
 783 56 56100 00100 023       04160 0        9999           276 Auburn CDP                                                   
 783 56 56100 00100 023       06045 0        9999           169 Bedford CDP                                                  
 783 56 56100 00100 023       15905 0        9999           506 Cokeville town                                               
 783 56 56100 00100 023       20110 0        9999           716 Diamondville town                                            
 783 56 56100 00100 023       25475 0        9999           123 Etna CDP                                                     
 783 56 56100 00100 023       25910 0        9999           277 Fairview CDP                                                 
 783 56 56100 00100 023       27650 0        9999            19 Fontenelle CDP                                               
 783 56 56100 00100 023       34030 0        9999           137 Grover CDP                                                   
 783 56 56100 00100 023       42005 0        9999          2651 Kemmerer city                                                
 783 56 56100 00100 023       43455 0        9999           431 La Barge town                                                
 783 56 56100 00100 023       57375 0        9999            18 Oakley CDP                                                   
 783 56 56100 00100 023       57810 0        9999           102 Opal town                                                    
 783 56 56100 00100 023       71440 0        9999           182 Smoot CDP                                                    
 783 56 56100 00100 023       73180 0        9999           776 Star Valley Ranch CDP                                        
 783 56 56100 00100 023       75572 0        9999            90 Taylor CDP                                                   
 783 56 56100 00100 023       76370 0        9999           341 Thayne town                                                  
 783 56 56100 00100 023       78400 0        9999           155 Turnerville CDP                                              
 783 56 56100 00100 023       99999 0        9999          5002 Remainder of Lincoln County                                  
 781 56 56100 00100 029                      9999         25786 Park County                                                  
 783 56 56100 00100 029       15760 0        9999          8835 Cody city                                                    
 783 56 56100 00100 029       29825 0        9999            29 Frannie town (part)                                          
 783 56 56100 00100 029       30985 0        9999            95 Garland CDP                                                  
 783 56 56100 00100 029       51720 0        9999           351 Meeteetse town                                               
 783 56 56100 00100 029       62450 0        9999          5373 Powell city                                                  
 783 56 56100 00100 029       63465 0        9999           233 Ralston CDP                                                  
 783 56 56100 00100 029       99999 0        9999         10870 Remainder of Park County                                     
 781 56 56100 00100 035                      9999          5920 Sublette County                                              
 783 56 56100 00100 035       07060 0        9999           408 Big Piney town                                               
 783 56 56100 00100 035       08365 0        9999           155 Bondurant CDP                                                
 783 56 56100 00100 035       09235 0        9999            30 Boulder CDP                                                  
 783 56 56100 00100 035       11845 0        9999             7 Calpet CDP                                                   
 783 56 56100 00100 035       17210 0        9999            76 Cora CDP                                                     
 783 56 56100 00100 035       18950 0        9999            89 Daniel CDP                                                   
 783 56 56100 00100 035       50415 0        9999           720 Marbleton town                                               
 783 56 56100 00100 035       61580 0        9999          1412 Pinedale town                                                
 783 56 56100 00100 035       99999 0        9999          3023 Remainder of Sublette County                                 
 781 56 56100 00100 037                      9999         37613 Sweetwater County                                            
 783 56 56100 00100 037       03652 0        9999            68 Arrowhead Springs CDP                                        
 783 56 56100 00100 037       04885 0        9999            97 Bairoil town                                                 
 783 56 56100 00100 037       15400 0        9999           850 Clearview Acres CDP                                          
 783 56 56100 00100 037       23010 0        9999           388 Eden CDP                                                     
 783 56 56100 00100 037       26055 0        9999           242 Farson CDP                                                   
 783 56 56100 00100 037       32870 0        9999           146 Granger town                                                 
 783 56 56100 00100 037       33740 0        9999         11808 Green River city                                             
 783 56 56100 00100 037       40265 0        9999           552 James Town CDP                                               
 783 56 56100 00100 037       46935 0        9999            56 Little America CDP                                           
 783 56 56100 00100 037       49400 0        9999            49 McKinnon CDP                                                 
 783 56 56100 00100 037       56700 0        9999          1974 North Rock Springs CDP                                       
 783 56 56100 00100 037       62160 0        9999             3 Point of Rocks CDP                                           
 783 56 56100 00100 037       62812 0        9999           413 Purple Sage CDP                                              
 783 56 56100 00100 037       65060 0        9999           665 Reliance CDP                                                 
 783 56 56100 00100 037       67235 0        9999         18708 Rock Springs city                                            
 783 56 56100 00100 037       74775 0        9999           244 Superior town                                                
 783 56 56100 00100 037       75137 0        9999            17 Sweeney Ranch CDP                                            
 783 56 56100 00100 037       75355 0        9999            82 Table Rock CDP                                               
 783 56 56100 00100 037       81300 0        9999           261 Wamsutter town                                               
 783 56 56100 00100 037       81703 0        9999            43 Washam CDP                                                   
 783 56 56100 00100 037       99999 0        9999           947 Remainder of Sweetwater County                               
 781 56 56100 00100 039                      9999         18251 Teton County                                                 
 783 56 56100 00100 039       01985 0        9999           400 Alta CDP                                                     
 783 56 56100 00100 039       37945 0        9999          1453 Hoback CDP                                                   
 783 56 56100 00100 039       40120 0        9999          8647 Jackson town                                                 
 783 56 56100 00100 039       54402 0        9999          1439 Moose Wilson Road CDP                                        
 783 56 56100 00100 039       63100 0        9999          1138 Rafter J Ranch CDP                                           
 783 56 56100 00100 039       71947 0        9999           864 South Park CDP                                               
 783 56 56100 00100 039       75935 0        9999           175 Teton Village CDP                                            
 783 56 56100 00100 039       83765 0        9999          1294 Wilson CDP                                                   
 783 56 56100 00100 039       99999 0        9999          2841 Remainder of Teton County                                    
 781 56 56100 00100 041                      9999         19742 Uinta County                                                 
 783 56 56100 00100 041       13005 0        9999             8 Carter CDP                                                   
 783 56 56100 00100 041       25620 0        9999         11507 Evanston city                                                
 783 56 56100 00100 041       27940 0        9999           400 Fort Bridger CDP                                             
 783 56 56100 00100 041       47370 0        9999            61 Lonetree CDP                                                 
 783 56 56100 00100 041       48675 0        9999          1938 Lyman town                                                   
 783 56 56100 00100 041       55345 0        9999          1153 Mountain View town                                           
 783 56 56100 00100 041       66655 0        9999            59 Robertson CDP                                                
 783 56 56100 00100 041       99999 0        9999          4616 Remainder of Uinta County                                    
 780 56 56100 00200                                      117976 PUMA 00200                                                   
 781 56 56100 00200 007                      9999         15639 Carbon County                                                
 783 56 56100 00200 007       04740 0        9999           348 Baggs town                                                   
 783 56 56100 00200 007       20690 0        9999            79 Dixon town                                                   
 783 56 56100 00200 007       24025 0        9999           192 Elk Mountain town                                            
 783 56 56100 00200 007       32650 0        9999           443 Grand Encampment town                                        
 783 56 56100 00200 007       35335 0        9999           873 Hanna town                                                   
 783 56 56100 00200 007       51575 0        9999           274 Medicine Bow town                                            
 783 56 56100 00200 007       63900 0        9999          8538 Rawlins city                                                 
 783 56 56100 00200 007       66075 0        9999            59 Riverside town                                               
 783 56 56100 00200 007       68685 0        9999          1726 Saratoga town                                                
 783 56 56100 00200 007       71150 0        9999           423 Sinclair town                                                
 783 56 56100 00200 007       99999 0        9999          2684 Remainder of Carbon County                                   
 781 56 56100 00200 013                      9999         35804 Fremont County                                               
 783 56 56100 00200 013       03000 0        9999          1766 Arapahoe CDP                                                 
 783 56 56100 00200 013       04015 0        9999            39 Atlantic City CDP                                            
 783 56 56100 00200 013       09307 0        9999           381 Boulder Flats CDP                                            
 783 56 56100 00200 013       18225 0        9999           163 Crowheart CDP                                                
 783 56 56100 00200 013       21415 0        9999           962 Dubois town                                                  
 783 56 56100 00200 013       25330 0        9999          1455 Ethete CDP                                                   
 783 56 56100 00200 013       28665 0        9999          1477 Fort Washakie CDP                                            
 783 56 56100 00200 013       38960 0        9999           407 Hudson town                                                  
 783 56 56100 00200 013       40555 0        9999           106 Jeffrey City CDP                                             
 783 56 56100 00200 013       40945 0        9999           236 Johnstown CDP                                                
 783 56 56100 00200 013       44760 0        9999          6867 Lander city                                                  
 783 56 56100 00200 013       60130 0        9999           165 Pavillion town                                               
 783 56 56100 00200 013       66220 0        9999          9310 Riverton city                                                
 783 56 56100 00200 013       70570 0        9999           635 Shoshoni town                                                
 783 56 56100 00200 013       99999 0        9999         11835 Remainder of Fremont County                                  
 781 56 56100 00200 025                      1350         66533 Natrona County                                               
 783 56 56100 00200 025       01115 0        1350            20 Alcova CDP                                                   
 783 56 56100 00200 025       02877 0        1350            88 Antelope Hills CDP                                           
 783 56 56100 00200 025       05245 0        1350           936 Bar Nunn town                                                
 783 56 56100 00200 025       06407 0        1350           170 Bessemer Bend CDP                                            
 783 56 56100 00200 025       10175 0        1350           192 Brookhurst CDP                                               
 783 56 56100 00200 025       13150 1        1350         49644 Casper city                                                  
 783 56 56100 00200 025       13222 0        1350           298 Casper Mountain CDP                                          
 783 56 56100 00200 025       23155 0        1350           169 Edgerton town                                                
 783 56 56100 00200 025       25765 0        1350          2255 Evansville town                                              
 783 56 56100 00200 025       35552 0        1350           682 Hartrandt CDP                                                
 783 56 56100 00200 025       38307 0        1350           214 Homa Hills CDP                                               
 783 56 56100 00200 025       51285 0        1350           181 Meadow Acres CDP                                             
 783 56 56100 00200 025       52445 0        1350           408 Midwest town                                                 
 783 56 56100 00200 025       53460 0        1350          2591 Mills town                                                   
 783 56 56100 00200 025       55200 0        1350           103 Mountain View CDP                                            
 783 56 56100 00200 025       62305 0        1350            51 Powder River CDP                                             
 783 56 56100 00200 025       64400 0        1350           439 Red Butte CDP                                                
 783 56 56100 00200 025       80575 0        1350          1008 Vista West CDP                                               
 783 56 56100 00200 025       99999 0        1350          7084 Remainder of Natrona County                                  
 780 56 56100 00300                                      140300 PUMA 00300                                                   
 781 56 56100 00300 003                      9999         11461 Big Horn County                                              
 783 56 56100 00300 003       05320 0        9999          1238 Basin town                                                   
 783 56 56100 00300 003       11120 0        9999           250 Burlington town                                              
 783 56 56100 00300 003       11700 0        9999           557 Byron town                                                   
 783 56 56100 00300 003       17645 0        9999           560 Cowley town                                                  
 783 56 56100 00300 003       19530 0        9999           177 Deaver town                                                  
 783 56 56100 00300 003       29825 0        9999           180 Frannie town (part)                                          
 783 56 56100 00300 003       33885 0        9999          1815 Greybull town                                                
 783 56 56100 00300 003       39395 0        9999            73 Hyattville CDP                                               
 783 56 56100 00300 003       47950 0        9999          2281 Lovell town                                                  
 783 56 56100 00300 003       49980 0        9999           104 Manderson town                                               
 783 56 56100 00300 003       51357 0        9999             8 Meadow Lark Lake CDP                                         
 783 56 56100 00300 003       99999 0        9999          4218 Remainder of Big Horn County                                 
 781 56 56100 00300 005                      9999         33698 Campbell County                                              
 783 56 56100 00300 005       02900 0        9999          1642 Antelope Valley-Crestview CDP                                
 783 56 56100 00300 005       31855 0        9999         19646 Gillette city                                                
 783 56 56100 00300 005       71350 0        9999          1177 Sleepy Hollow CDP                                            
 783 56 56100 00300 005       85015 0        9999          1347 Wright town                                                  
 783 56 56100 00300 005       99999 0        9999          9886 Remainder of Campbell County                                 
 781 56 56100 00300 009                      9999         12052 Converse County                                              
 783 56 56100 00300 009       21125 0        9999          5288 Douglas city                                                 
 783 56 56100 00300 009       25185 0        9999            32 Esterbrook CDP                                               
 783 56 56100 00300 009       32435 0        9999          2231 Glenrock town                                                
 783 56 56100 00300 009       47805 0        9999             1 Lost Springs town                                            
 783 56 56100 00300 009       67440 0        9999           449 Rolling Hills town                                           
 783 56 56100 00300 009       99999 0        9999          4051 Remainder of Converse County                                 
 781 56 56100 00300 011                      9999          5887 Crook County                                                 
 783 56 56100 00300 011       39105 0        9999           408 Hulett town                                                  
 783 56 56100 00300 011       54185 0        9999           807 Moorcroft town                                               
 783 56 56100 00300 011       61610 0        9999           222 Pine Haven town                                              
 783 56 56100 00300 011       74195 0        9999          1161 Sundance town                                                
 783 56 56100 00300 011       99999 0        9999          3289 Remainder of Crook County                                    
 781 56 56100 00300 015                      9999         12538 Goshen County                                                
 783 56 56100 00300 015       28230 0        9999           243 Fort Laramie town                                            
 783 56 56100 00300 015       36060 0        9999            69 Hawk Springs CDP                                             
 783 56 56100 00300 015       39250 0        9999            21 Huntley CDP                                                  
 783 56 56100 00300 015       43745 0        9999           332 La Grange town                                               
 783 56 56100 00300 015       46790 0        9999           510 Lingle town                                                  
 783 56 56100 00300 015       77530 0        9999          5776 Torrington city                                              
 783 56 56100 00300 015       80285 0        9999            28 Veteran CDP                                                  
 783 56 56100 00300 015       86665 0        9999           169 Yoder town                                                   
 783 56 56100 00300 015       99999 0        9999          5390 Remainder of Goshen County                                   
 781 56 56100 00300 017                      9999          4882 Hot Springs County                                           
 783 56 56100 00300 017       22720 0        9999           274 East Thermopolis town                                        
 783 56 56100 00300 017       42730 0        9999            57 Kirby town                                                   
 783 56 56100 00300 017       48240 0        9999           525 Lucerne CDP                                                  
 783 56 56100 00300 017       59332 0        9999            11 Owl Creek CDP                                                
 783 56 56100 00300 017       76515 0        9999          3172 Thermopolis town                                             
 783 56 56100 00300 017       99999 0        9999           843 Remainder of Hot Springs County                              
 781 56 56100 00300 019                      9999          7075 Johnson County                                               
 783 56 56100 00300 019       10685 0        9999          3900 Buffalo city                                                 
 783 56 56100 00300 019       41353 0        9999           249 Kaycee town                                                  
 783 56 56100 00300 019       99999 0        9999          2926 Remainder of Johnson County                                  
 781 56 56100 00300 027                      9999          2407 Niobrara County                                              
 783 56 56100 00300 027       44615 0        9999            51 Lance Creek CDP                                              
 783 56 56100 00300 027       48530 0        9999          1447 Lusk town                                                    
 783 56 56100 00300 027       50270 0        9999           101 Manville town                                                
 783 56 56100 00300 027       79705 0        9999            18 Van Tassell town                                             
 783 56 56100 00300 027       99999 0        9999           790 Remainder of Niobrara County                                 
 781 56 56100 00300 031                      9999          8807 Platte County                                                
 783 56 56100 00300 031       14097 0        9999           132 Chugcreek CDP                                                
 783 56 56100 00300 031       14165 0        9999           244 Chugwater town                                               
 783 56 56100 00300 031       32290 0        9999           229 Glendo town                                                  
 783 56 56100 00300 031       34320 0        9999          1147 Guernsey town                                                
 783 56 56100 00300 031       35625 0        9999            76 Hartville town                                               
 783 56 56100 00300 031       44252 0        9999            77 Lakeview North CDP                                           
 783 56 56100 00300 031       71295 0        9999            82 Slater CDP                                                   
 783 56 56100 00300 031       82967 0        9999            67 Westview Circle CDP                                          
 783 56 56100 00300 031       83040 0        9999          3548 Wheatland town                                               
 783 56 56100 00300 031       86737 0        9999           242 Y-O Ranch CDP                                                
 783 56 56100 00300 031       99999 0        9999          2963 Remainder of Platte County                                   
 781 56 56100 00300 033                      9999         26560 Sheridan County                                              
 783 56 56100 00300 033       03725 0        9999            33 Arvada CDP                                                   
 783 56 56100 00300 033       06770 0        9999           198 Big Horn CDP                                                 
 783 56 56100 00300 033       15325 0        9999           115 Clearmont town                                               
 783 56 56100 00300 033       19385 0        9999           678 Dayton town                                                  
 783 56 56100 00300 033       59985 0        9999           137 Parkman CDP                                                  
 783 56 56100 00300 033       63755 0        9999           701 Ranchester town                                              
 783 56 56100 00300 033       69845 0        9999         15804 Sheridan city                                                
 783 56 56100 00300 033       73615 0        9999           887 Story CDP                                                    
 783 56 56100 00300 033       99999 0        9999          8007 Remainder of Sheridan County                                 
 781 56 56100 00300 043                      9999          8289 Washakie County                                              
 783 56 56100 00300 043       00417 0        9999           297 Airport Road CDP                                             
 783 56 56100 00300 043       49472 0        9999           278 Mc Nutt CDP                                                  
 783 56 56100 00300 043       71765 0        9999           374 South Flat CDP                                               
 783 56 56100 00300 043       75790 0        9999           304 Ten Sleep town                                               
 783 56 56100 00300 043       81671 0        9999           604 Washakie Ten CDP                                             
 783 56 56100 00300 043       82677 0        9999           321 West River CDP                                               
 783 56 56100 00300 043       83910 0        9999            60 Winchester CDP                                               
 783 56 56100 00300 043       84925 0        9999          5250 Worland city                                                 
 783 56 56100 00300 043       99999 0        9999           801 Remainder of Washakie County                                 
 781 56 56100 00300 045                      9999          6644 Weston County                                                
 783 56 56100 00300 045       37727 0        9999           166 Hill View Heights CDP                                        
 783 56 56100 00300 045       56215 0        9999          3065 Newcastle city                                               
 783 56 56100 00300 045       58680 0        9999           215 Osage CDP                                                    
 783 56 56100 00300 045       79125 0        9999           872 Upton town                                                   
 783 56 56100 00300 045       99999 0        9999          2326 Remainder of Weston County                                   
 780 56 56100 00400                                      113621 PUMA 00400                                                   
 781 56 56100 00400 001                      9999         32014 Albany County                                                
 783 56 56100 00400 001       00680 0        9999            80 Albany CDP                                                   
 783 56 56100 00400 001       13440 0        9999           191 Centennial CDP                                               
 783 56 56100 00400 001       45050 0        9999         27204 Laramie city                                                 
 783 56 56100 00400 001       67090 0        9999           235 Rock River town                                              
 783 56 56100 00400 001       76467 0        9999            31 The Buttes CDP                                               
 783 56 56100 00400 001       84852 0        9999           100 Woods Landing-Jelm CDP                                       
 783 56 56100 00400 001       99999 0        9999          4173 Remainder of Albany County                                   
 781 56 56100 00400 021                      1580         81607 Laramie County                                               
 783 56 56100 00400 021       00825 0        1580           120 Albin town                                                   
 783 56 56100 00400 021       11265 0        1580           285 Burns town                                                   
 783 56 56100 00400 021       13900 1        1580         53011 Cheyenne city                                                
 783 56 56100 00400 021       29300 0        1580          3272 Fox Farm-College CDP                                         
 783 56 56100 00400 021       61435 0        1580          1153 Pine Bluffs town                                             
 783 56 56100 00400 021       63800 0        1580          4869 Ranchettes CDP                                               
 783 56 56100 00400 021       71800 0        1580          4201 South Greeley CDP                                            
 783 56 56100 00400 021       81640 0        1580          4440 Warren AFB CDP                                               
 783 56 56100 00400 021       99999 0        1580         10256 Remainder of Laramie County                                  
