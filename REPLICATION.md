# [ NO MC number] [IMPERFECT EXAMPLE: A TEST OF REPRODUCIBILITY] Validation and Replication results

SUMMARY
-------

Data description
----------------
- Data Source: [Census of Population and Housing, 2000 [United States]: Public Use Microdata Sample: 5-Percent Sample (ICPSR 13568)](https://doi.org/10.3886/ICPSR13568. v1.)
- Permissions: "The public-use data files in this collection are available for access by the general public. Access does not require affiliation with an ICPSR member institution."
(from the DOI link)

### Analysis Data Files
- [ ] No analysis data file mentioned
- [ ] Analysis data files mentioned, not provided (explain reasons below)
- [x] Analysis data files mentioned, provided. File names listed below.

Filenames:
```
00_setup_stata.do
01_dataclean.do
02_table1.do
config.do
master.do
```


### ICPSR data deposit

- [ ] JEL Classification (required)
- [ ] Manuscript Number (required)
- [ ] Subject Terms (highly recommended)
- [ ] Geographic coverage (highly recommended)
- [ ] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [ ] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [ ] Units of Observation (suggested)

- [REQUIRED] Please update the following fields listed in this report. 
- JEL Classification  
- Manuscript Number 


Data checks
-----------

- Data can be read using the software provided by the author(stata) and is in archive-ready formats.(txt)
- Data has no variable labels,
- No sensitive data found after running PII


Code description
----------------


- Five Stata do files, including:
"master.do": runs all the programs sequentially
"config.do" : used for env setup
"00_setup_stat.do": sets up dir structure and installs all required packages
"01_dataclean.do": reads in downloaded data, saves person and household files and creates a clean and merged dataset
"02_table1.do": creates table 1


- Table 1: produced by lines 5-12 in program '02_table1.do'
- no figures or in-text numbers found in the manuscript

Stated Requirements
---------------------
- [x] Software Requirements specified as follows:
       - Stata  
- "Runtime: approximatly 1.6s on 2016 vintage Linux workstation."

Replication steps
-----------------
1. Downloaded code from URL provided.
2. Downloaded data from URL indicated in the README. A sign-up was required (not indicated in README)
3. Modified global basepath in config to reflect my localenv
4. Installed latab manually and created outputdata folder and tables folder
4. Opened master.do in stata and ran as required, generated log file
5. Table 1 not similar to the one provided is generated.

Computing Environment
---------------------

- Mac Laptop, 8 GB of memory
- CISER Shared Windows Server 2016, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (3 processors)


Findings
--------

### Data Preparation Code
- I had to manually install latab and outreg2 for the program to run.
- Manually created tables and output folders before I could get a sensible output log with no errors and a tex table.
- Program `master.do` produced a .tex file for table 1.

### Tables
- Table 1: Could not replicate this table

Classification
--------------


- [ ] full replication
- [ ] full replication with minor issues
- [ ] partial replication (see above)
- [x] not able to replicate most or all of the results (reasons see above)

[required] Author to please provide debugged code that solves/clarifies the issues identified in the above report.
